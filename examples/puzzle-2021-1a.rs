extern crate rust_advent_of_code;
use rust_advent_of_code::depth::increase_count;
use std::fs::File;
use std::io::{self, BufRead};
use std::iter::FromIterator;

fn main() {
    let lines = io::BufReader::new(File::open("./puzzle-2021-1.txt").expect("Could not open file"))
        .lines()
        .filter_map(Result::ok);

    println!(
        "{}",
        increase_count(Vec::from_iter(lines.filter_map(|l| l.parse::<u64>().ok()))).to_string()
    );
}
