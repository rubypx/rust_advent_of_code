extern crate rust_advent_of_code;
use rust_advent_of_code::depth::{parse_instructions, position_from_instructions};
use std::fs::File;
use std::io::{self, BufRead};
use std::iter::FromIterator;

fn main() {
    let lines = io::BufReader::new(File::open("./puzzle-2021-2.txt").expect("Could not open file"))
        .lines()
        .filter_map(Result::ok);

    let final_position = position_from_instructions(parse_instructions(Vec::from_iter(lines)));
    println!("{:?}", final_position.0 * final_position.1);
}
