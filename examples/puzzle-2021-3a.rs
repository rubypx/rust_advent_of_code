extern crate rust_advent_of_code;
use rust_advent_of_code::diagnostics::{bit_vote, parse_metrics, bits_to_str};
use std::fs::File;
use std::io::{self, BufRead};
use std::iter::FromIterator;

fn main() {
    let lines = Vec::from_iter(
        io::BufReader::new(File::open("./puzzle-2021-3.txt").expect("Could not open file"))
            .lines()
            .filter_map(Result::ok),
    );
    let line_count = lines.len();
    let bit_vote_positive = &bit_vote(parse_metrics(lines), 12, line_count);
    let bit_vote_negative = bit_vote_positive
        .into_iter()
        .map(|bit| !bit)
        .collect::<Vec<bool>>();

    let gamma_rate = isize::from_str_radix(&bits_to_str(&bit_vote_positive), 2).unwrap();
    let epsilon_rate = isize::from_str_radix(&bits_to_str(&bit_vote_negative), 2).unwrap();
    println!("{:?}", gamma_rate * epsilon_rate);
}
