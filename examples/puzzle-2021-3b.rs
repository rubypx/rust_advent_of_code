extern crate rust_advent_of_code;
use rust_advent_of_code::diagnostics::{filter_by_popularity, parse_metrics, bits_to_str};
use std::fs::File;
use std::io::{self, BufRead};
use std::iter::FromIterator;

fn main() {
    let lines = Vec::from_iter(
        io::BufReader::new(File::open("./puzzle-2021-3.txt").expect("Could not open file"))
            .lines()
            .filter_map(Result::ok),
    );
    let parsed = parse_metrics(lines);
    let oxygen_generator_rating =
        isize::from_str_radix(&bits_to_str(&filter_by_popularity(&parsed, 12, 1)), 2).unwrap();
    let co2_scrubber_rating =
        isize::from_str_radix(&bits_to_str(&filter_by_popularity(&parsed, 12, 0)), 2).unwrap();
    println!("oxygen generator rating: {}\nCO2 scrubber rating: {}\nlife support rating: {}", oxygen_generator_rating, co2_scrubber_rating, oxygen_generator_rating * co2_scrubber_rating);
}
