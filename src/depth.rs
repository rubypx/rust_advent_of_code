#[cfg(test)]
mod tests {
    use super::{
        increase_count, increase_count_averaged, parse_instructions, position_from_instructions,
        position_from_instructions_using_aim,
    };

    #[test]
    fn returns_increased_value() {
        let expected: u64 = 7;
        assert_eq!(
            expected,
            increase_count(vec![199, 200, 208, 210, 200, 207, 240, 269, 260, 263])
        );
    }

    #[test]
    fn returns_increased_averaged_value() {
        let expected: u64 = 5;
        assert_eq!(
            expected,
            increase_count_averaged(vec![199, 200, 208, 210, 200, 207, 240, 269, 260, 263])
        );
    }
    #[test]
    fn returns_correct_position() {
        let expected = (15i64, 10i64);
        assert_eq!(
            expected,
            position_from_instructions(parse_instructions(
                "forward 5
down 5
forward 8
up 3
down 8
forward 2"
                    .lines()
                    .map(String::from)
                    .collect()
            ))
        );
    }
    #[test]
    fn returns_correct_position_using_aim() {
        let expected = (15i64, 60i64);
        assert_eq!(
            expected,
            position_from_instructions_using_aim(parse_instructions(
                "forward 5
down 5
forward 8
up 3
down 8
forward 2"
                    .lines()
                    .map(String::from)
                    .collect()
            ))
        );
    }
}

#[derive(Debug, PartialEq)]
enum Direction {
    Forward,
    Up,
    Down,
}

#[derive(Debug, PartialEq)]
pub struct Instruction {
    direction: Direction,
    distance: u64,
}

pub fn position_from_instructions_using_aim(instructions: Vec<Instruction>) -> (i64, i64) {
    let mut aim = 0;
    instructions
        .into_iter()
        .map(|instruction| {
            let distance = instruction.distance as i64;
            match instruction.direction {
                Direction::Up => {
                    aim += -distance;
                    (0, 0)
                }
                Direction::Down => {
                    aim += distance;
                    (0, 0)
                }
                Direction::Forward => (distance, aim * distance),
            }
        })
        .fold((0, 0), |acc, coords| (acc.0 + coords.0, acc.1 + coords.1))
}

pub fn position_from_instructions(instructions: Vec<Instruction>) -> (i64, i64) {
    instructions
        .into_iter()
        .map(|instruction| {
            let distance = instruction.distance as i64;
            match instruction.direction {
                Direction::Up => (0, -distance),
                Direction::Down => (0, distance),
                Direction::Forward => (distance, 0),
            }
        })
        .fold((0, 0), |acc, coords| (acc.0 + coords.0, acc.1 + coords.1))
}

pub fn parse_instructions(instructions: Vec<String>) -> Vec<Instruction> {
    instructions
        .into_iter()
        .map(|i| {
            let (direction_str, distance_str) = i
                .split_once(" ")
                .expect(&format!("Could not parse instruction: {}", i));
            let distance = distance_str
                .parse::<u64>()
                .expect(format!("Cannot parse string [{}] to u64", distance_str).as_str());

            let direction = match direction_str {
                "forward" => Direction::Forward,
                "up" => Direction::Up,
                "down" => Direction::Down,
                _ => panic!("Unable to parse direction from: [{}]", direction_str),
            };

            Instruction {
                direction,
                distance,
            }
        })
        .collect()
}

pub fn increase_count(depths: Vec<u64>) -> u64 {
    let mut previous_depth = depths[0];
    let mut depth_increase_counter = 0;
    for depth in depths {
        if depth > previous_depth {
            depth_increase_counter += 1;
        }
        previous_depth = depth;
    }
    depth_increase_counter
}

pub fn increase_count_averaged(depths: Vec<u64>) -> u64 {
    let depth_groups = depths.windows(3);
    let mut first = true;
    let mut previous_depth = 0;
    let mut depth_increase_counter = 0;
    for depth_group in depth_groups {
        let group_sum = depth_group.iter().sum();
        if group_sum > previous_depth && !first {
            depth_increase_counter += 1;
        }
        previous_depth = group_sum;
        first = false;
    }
    depth_increase_counter
}
