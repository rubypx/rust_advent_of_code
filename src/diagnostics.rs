pub fn bits_to_str(bits: &Vec<bool>) -> String {
    bits.into_iter()
        .map(|bit| (*bit as u8).to_string())
        .collect::<Vec<_>>()
        .join("")
}

fn total_for_index(index: usize, metrics: &Vec<Vec<bool>>) -> usize {
    metrics
        .into_iter()
        .fold(0usize, |acc, metric| acc + metric[index] as usize)
}

pub fn filter_by_popularity(metrics: &Vec<Vec<bool>>, length: usize, target: u8) -> Vec<bool> {
    let mut filtered = metrics.clone();
    for index in 0..length {
        let total = total_for_index(index, &filtered) as f32;
        let half_len = filtered.len() as f32 / 2.0;
        let filter_by: u8;
        filter_by = match target {
            1 => {
                if total >= half_len {
                    1
                } else {
                    0
                }
            }
            0 => {
                if total < half_len {
                    1
                } else {
                    0
                }
            }
            // TODO: Restrict target parameter to 0 or 1 values (or perhaps enum of {most|least})
            _ => panic!("invalid target [{}]", target),
        };
        filtered = filtered
            .into_iter()
            .filter(|metric| metric[index] as u8 == filter_by)
            .collect();
        if filtered.len() == 1 {
            return filtered[0].clone();
        }
    }
    panic!("unable to filter metrics")
}

pub fn bit_vote(metrics: Vec<Vec<bool>>, length: usize, line_count: usize) -> Vec<bool> {
    let mut totals = vec![0; length];
    metrics.into_iter().for_each(|metric| {
        metric
            .into_iter()
            .enumerate()
            .for_each(|(index, bit)| totals[index] += bit as u64)
    });
    totals
        .into_iter()
        .map(|total| total > (line_count as u64 / 2))
        .collect()
}

pub fn parse_metric(metric: String) -> Vec<bool> {
    metric
        .chars()
        .into_iter()
        .map(|char| match char {
            '1' => true,
            '0' => false,
            _ => panic!("[{}] could not be converted to boolean", char),
        })
        .collect()
}

pub fn parse_metrics(metrics: Vec<String>) -> Vec<Vec<bool>> {
    metrics.into_iter().map(parse_metric).collect()
}
